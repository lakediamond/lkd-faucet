package main

import (
	"lkd-faucet/internal/app/faucet/httpserver"
	"lkd-faucet/internal/pkg/application"
)

func main() {
	app := application.NewApplication()
	httpserver.Serve(app)
}
