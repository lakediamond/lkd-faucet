package httpserver

import (
	"fmt"
	"github.com/justinas/alice"
	"github.com/rs/zerolog/log"
	"lkd-faucet/internal/pkg/application"
	"lkd-faucet/pkg/handlers"
	"net/http"
)

// Serve will run HTTP Server on configured application PORT
func Serve(app *application.Application) {
	router := NewHTTPRouter(app)
	routes := alice.New(handlers.JSONContentTypeHandler, handlers.LoggingHandler, handlers.RecoveryHandler).Then(router)

	addr := fmt.Sprintf(":%s", app.Config.Port)
	log.Info().Msgf("Serving API on http://localhost%s", addr)
	if err := http.ListenAndServe(addr, routes); err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
