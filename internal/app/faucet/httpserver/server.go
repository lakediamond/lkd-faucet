package httpserver

import (
	"github.com/julienschmidt/httprouter"
	"lkd-faucet/internal/app/faucet/ethereum"
	"lkd-faucet/internal/pkg/application"
	"net/http"
)

// NewHTTPRouter will return application router.
// Require instance of application to properly mount routes to application and pass application context to HTTP Handlers
func NewHTTPRouter(app *application.Application) *httprouter.Router {
	router := httprouter.New()

	routes := application.Routes{
		application.Route{Alias: "request_ether", Method: "POST", Path: "/request_ether", Handler: app.WithApplicationContext(ethereum.RequestEtherHandler)},
		application.Route{Alias: "request_tokens", Method: "POST", Path: "/request_tokens", Handler: app.WithApplicationContext(ethereum.RequestTokensHandler)},
		application.Route{Alias: "/", Method: "GET", Path: "/", Handler: http.FileServer(http.Dir("./static"))},
		application.Route{Alias: "/", Method: "GET", Path: "/app.js", Handler: http.FileServer(http.Dir("./static"))},
	}
	for _, route := range routes {
		router.Handler(route.Method, route.Path, route.Handler)
	}

	app.MountRoutes(routes)

	return router
}

//http.HandleFunc("/requestEther", handlers.RequestEtherHandler)
//http.HandleFunc("/requestTokens", handlers.RequestTokensHandler)
//
//http.ListenAndServe(":8000", nil)
