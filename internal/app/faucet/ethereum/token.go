package ethereum

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"strconv"

	"lkd-faucet/internal/pkg/application"
	contextApp "lkd-faucet/internal/pkg/context"
	"lkd-faucet/pkg/handlers"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/rs/zerolog/log"
	"golang.org/x/net/context"
)

var RequestTokensHandler = handlers.ApplicationHandler(requestTokens)

func requestTokens(w http.ResponseWriter, r *http.Request) {
	app := contextApp.GetApplicationContext(r)

	// Cloudflare CF-IPCountry header tracking
	for name, values := range r.Header {
		for _, value := range values {
			log.Debug().Msg(fmt.Sprintf("Cloudflare CF-IPCountry header tracking, [header %s ; value %s]", name, value))
		}
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		//log.Println("error read body:", err)
		return
	}
	var respData []byte
	txHash, err := sendTokens(app, string(data))
	if txHash == "" {
		respData, _ = json.Marshal("Await for previous transaction")
	} else {
		respData, _ = json.Marshal(txHash)
	}
	w.WriteHeader(200)
	w.Write(respData)
}

func sendTokens(app *application.Application, to string) (string, error) {
	if !checkTokenGreedy(app, to) {
		return "User too greedy", nil
	}

	nonce, err := app.SupplyData.Client.PendingNonceAt(context.Background(), common.HexToAddress(app.SupplyData.FausetPublicKey))
	if err != nil {
		log.Error().Err(err)
	}

	gasPrice, err := app.SupplyData.Client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Error().Err(err)
	}

	auth := bind.NewKeyedTransactor(app.SupplyData.PrivateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)     // in wei
	auth.GasLimit = uint64(300000) // in units
	auth.GasPrice = gasPrice

	tx, err := app.SupplyData.Token.Transfer(auth, common.HexToAddress(to), big.NewInt(50000))
	if err != nil {
		return "", err
	}
	return tx.Hash().Hex(), nil
}

func checkTokenGreedy(app *application.Application, address string) bool {

	balance, err := app.SupplyData.Token.BalanceOf(app.SupplyData.CallOpts, common.HexToAddress(address))
	if err != nil {
		log.Error().Err(err)
	}
	maxTokens, err := strconv.ParseInt(app.Config.MaxTokens, 10, 64)

	if balance.Cmp(big.NewInt(maxTokens)) == -1 {
		return true
	}

	return false
}
