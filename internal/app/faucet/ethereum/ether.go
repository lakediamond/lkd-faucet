package ethereum

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lkd-faucet/internal/pkg/application"
	contextApp "lkd-faucet/internal/pkg/context"
	"lkd-faucet/pkg/handlers"
	"math/big"
	"net/http"
	"strconv"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/rs/zerolog/log"
	"golang.org/x/net/context"
)

var RequestEtherHandler = handlers.ApplicationHandler(requestEther)

func requestEther(w http.ResponseWriter, r *http.Request) {
	app := contextApp.GetApplicationContext(r)

	// Cloudflare CF-IPCountry header tracking
	for name, values := range r.Header {
		for _, value := range values {
			log.Debug().Msg(fmt.Sprintf("Cloudflare CF-IPCountry header tracking, [header %s ; value %s]", name, value))
		}
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		//log.Error().Err(err).Msg("error read body")
		return
	}
	var respData []byte
	txHash, err := sendEther(app, string(data))
	if txHash == "" {
		respData, _ = json.Marshal("Await for previous transaction")
	} else {
		respData, _ = json.Marshal(txHash)
	}
	w.WriteHeader(200)
	w.Write(respData)
}

func sendEther(app *application.Application, to string) (string, error) {
	if !checkEthGreedy(app, to) {
		return "User too greedy", nil
	}

	nonce, err := app.SupplyData.Client.PendingNonceAt(context.Background(), common.HexToAddress(app.SupplyData.FausetPublicKey))
	if err != nil {
		log.Error().Err(err)
		return "", err
	}

	value := big.NewInt(1000000000000000000) // in wei (1 eth)
	gasLimit := uint64(21000)                // in units
	gasPrice, err := app.SupplyData.Client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Error().Err(err)
		return "", err
	}

	toAddress := common.HexToAddress(to)
	var data []byte
	tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, data)

	chainID, err := app.SupplyData.Client.NetworkID(context.Background())
	if err != nil {
		log.Error().Err(err)
		return "", err
	}

	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), app.SupplyData.PrivateKey)
	if err != nil {
		log.Error().Err(err)
		return "", err
	}

	ts := types.Transactions{signedTx}
	rawTx := hex.EncodeToString(ts.GetRlp(0))

	rawTxBytes, err := hex.DecodeString(rawTx)
	rlp.DecodeBytes(rawTxBytes, &tx)

	err = app.SupplyData.Client.SendTransaction(context.Background(), tx)
	if err != nil {
		log.Error().Err(err)
		return "", err
	}

	log.Debug().Msg(signedTx.Hash().Hex())
	return signedTx.Hash().Hex(), nil
}

func checkEthGreedy(app *application.Application, address string) bool {
	balance, err := app.SupplyData.Client.BalanceAt(context.Background(), common.HexToAddress(address), nil)
	if err != nil {
		log.Error().Err(err)
	}
	log.Debug().Msgf("%d", balance)
	maxEther, err := strconv.ParseInt(app.Config.MaxEther, 10, 64)
	if balance.Cmp(big.NewInt(maxEther)) == -1 {
		return true
	}

	return false
}
