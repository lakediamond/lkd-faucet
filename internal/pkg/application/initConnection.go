package application

import (
	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
	"lkd-faucet/token"
	"golang.org/x/net/context"
	"time"
)

func initConnection(app *Application) {
	createRpcConnection(app)
	initFauset(app)
}

func createRpcConnection(app *Application) {
	ethConn, err := ethclient.Dial(app.Config.RpcPort)
	if err != nil {
		log.Error().Err(err).Msg("Blockchain connection broken. Reconnecting in 5 second")
		time.Sleep(5000 * time.Millisecond)
		createRpcConnection(app)
		return
	}
	app.SupplyData.Client = ethConn
}

func initFauset(app *Application) {
	app.SupplyData.PrivateKey, _ = crypto.HexToECDSA(app.Config.FaucetPrivateKey)

	publicKey := app.SupplyData.PrivateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Error().Msg("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	app.SupplyData.FausetPublicKey = fromAddress.String()

	instance, err := token.NewToken(common.HexToAddress(app.Config.TokenAddress), app.SupplyData.Client)
	if err != nil {
		log.Error().Msg("Cannot initialize new token")
	}
	app.SupplyData.Token = instance

	app.SupplyData.CallOpts = &bind.CallOpts{Pending: true, From: common.HexToAddress(app.SupplyData.FausetPublicKey), Context: context.Background()}
}
