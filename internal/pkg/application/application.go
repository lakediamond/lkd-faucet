package application

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/ethclient"
	"lkd-faucet/token"

	"github.com/joho/godotenv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Config as application Config type
type Config struct {
	LogLevel         string
	Port             string
	RpcPort          string
	FaucetPrivateKey string
	TokenAddress     string
	MaxEther         string
	MaxTokens        string
}

type Application struct {
	Config     Config
	routes     Routes
	SupplyData SupplyData
}

type key int

const (
	ApplicationKey key = 0
)

type Route struct {
	Alias   string
	Method  string
	Path    string
	Handler http.Handler
}

type SupplyData struct {
	Client          *ethclient.Client
	PrivateKey      *ecdsa.PrivateKey
	FausetPublicKey string
	CallOpts        *bind.CallOpts
	Token           *token.Token
}

// Routes represents list of routes
type Routes []Route

// NewApplication return application instance
func NewApplication() *Application {
	config := compileConfig()
	app := Application{config,
		nil, SupplyData{}}
	initConnection(&app)
	return &app
}

func (app *Application) WithApplicationContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), ApplicationKey, app)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (app *Application) MountRoutes(routes Routes) {
	app.routes = routes
}

func (app *Application) FindRoute(alias string) string {
	var found string
	var supportedAliases []string
	for _, route := range app.routes {
		supportedAliases = append(supportedAliases, route.Alias)
		if route.Alias == alias {
			found = route.Path
		}
	}
	if found == "" {
		panic(fmt.Errorf("route with alias %s not found. Supported aliases: %s", alias, supportedAliases))
	}
	return found
}

func VCSDir() (string, error) {
	wd, _ := os.Getwd()
	return findVCS(wd, 10)
}

func findVCS(dir string, nestLevel int) (string, error) {
	if nestLevel == 0 {
		return "", os.ErrNotExist
	}
	_, err := os.Stat(fmt.Sprintf("%s/.git", dir))
	if err != nil {
		if os.IsNotExist(err) {
			return findVCS(fmt.Sprintf("%s/..", dir), nestLevel-1)
		}
		return "", os.ErrNotExist
	}
	return dir, nil
}

func getEnv() map[string]string {
	env := os.Getenv("ENV")
	if env == "" {
		env = "default"
	}
	var myEnv = make(map[string]string, 0)

	if env != "docker" {
		wd, err := VCSDir()
		if err != nil {
			log.Panic().Msgf("Can't find project root %s", wd)
		}
		file := fmt.Sprintf("%s/configs/.%s", wd, env)
		godotenv.Load()
		err = godotenv.Load(file)
		if err != nil {
			log.Panic().Msgf("Can't load env file, create %s/.%s file in configs folder", wd, env)
		}
		myEnv, err = godotenv.Read(file)
		if err != nil {
			log.Panic().Msgf("Can't read .%s file", env)
		}

	} else {
		for _, e := range os.Environ() {
			pair := strings.Split(e, "=")
			myEnv[pair[0]] = pair[1]
		}
	}

	return myEnv
}

func compileConfig() Config {
	myEnv := getEnv()

	return Config{
		LogLevel:         myEnv["LOG_LEVEL"],
		Port:             myEnv["PORT"],
		RpcPort:          myEnv["RPC_PORT"],
		FaucetPrivateKey: myEnv["FAUCET_PRIVATE_KEY"],
		TokenAddress:     myEnv["TOKEN_ADDRESS"],
		MaxEther:         myEnv["MAX_ETHER"],
		MaxTokens:        myEnv["MAX_TOKENS"],
	}
}

func (app *Application) SetLoggerLevel() {
	level, err := zerolog.ParseLevel(app.Config.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Info().Msgf("Log level set to %s", level.String())
}
