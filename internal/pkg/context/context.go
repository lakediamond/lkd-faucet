package context

import (
	"lkd-faucet/internal/pkg/application"
	"net/http"
)

func GetApplicationContext(r *http.Request) *application.Application {
	return r.Context().Value(application.ApplicationKey).(*application.Application)
}
