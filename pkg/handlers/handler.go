package handlers

import (
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type recoveryLogger struct {
	zerolog.Logger
}

func (log recoveryLogger) Println(v ...interface{}) {
	log.Panic().Msg(fmt.Sprint(v...))
}

var RecoveryHandler = handlers.RecoveryHandler(
	handlers.RecoveryLogger(recoveryLogger{log.Logger}),
	handlers.PrintRecoveryStack(false))

type httpLoggingHandler struct {
	handler http.Handler
}

func (h httpLoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	h.handler.ServeHTTP(w, r)
	elapsed := time.Since(start)
	log.Info().Str("path", r.URL.Path).Str("method", r.Method).Str("took", elapsed.String()).Msg("")
}

func LoggingHandler(h http.Handler) http.Handler {
	return httpLoggingHandler{h}
}

type applicationHanler struct {
	handler http.HandlerFunc
}

func JSONContentTypeHandler(h http.Handler) http.Handler {
	return handlers.ContentTypeHandler(h, "application/json", "application/rawdata")
}

func (c applicationHanler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.handler(w, r)
}

func ApplicationHandler(h http.HandlerFunc) http.Handler {
	return applicationHanler{h}
}
